﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aircash.Config
{
   public class Configuration
    {
        public string PartnerId { get; set; }
        public string PrivateKeyPath { get; set; }
        public string PrivateKeyPass { get; set; }
    }
}
