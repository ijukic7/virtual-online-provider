﻿using System;

namespace Aircash.Config
{
    public class AbonConfiguration
    {
        public string AbonSoapUri { get; set; }
        public string ValidateCouponSoapAction { get; set; }
        public string ValidateCouponSoapEnvelope { get; set; }
        public string ConfirmTransactionSoapAction { get; set; }
        public string ConfirmTransactionSoapEnvelope { get; set; }
        public string CreateCouponSoapAction { get; set; }
        public string CreateCouponSoapEnvelope { get; set; }
        public string PrivateKeyPath { get; set; }
        public string PrivateKeyPass { get; set; }
        public string CancelCouponSoapEnvelope { get; set; }
        public string CancelCouponSoapAction { get; set; }
        public string AbonSoapUriSalePartner { get; set; }
    }
}
