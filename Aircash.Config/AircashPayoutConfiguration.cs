﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aircash.Config
{
    public class AircashPayoutConfiguration
    {
        public string AircashPayoutBaseUri { get; set; }
        public string CheckUserUrl { get; set;}
        public string CreatePayoutUrl { get; set; }
        public string CheckTransactionStatusUrl { get; set; }
    }
}
