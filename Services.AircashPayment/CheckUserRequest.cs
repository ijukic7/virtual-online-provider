﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.AircashPayout
{
    public class CheckUserRequest
    {
        public Guid PartnerID { get; set; }
        public string PersonalID { get; set; }
        public string PhoneNumber { get; set; }
        public string Signature { get; set; }
    }
}
