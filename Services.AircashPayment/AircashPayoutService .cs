﻿using Aircash.Config;
using Microsoft.Extensions.Options;
using Services.Signature;
using Services.HttpRequest;
using Services.TemplateSystem;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Services.AircashPayout;

namespace Services.AircashPayout
{
    public class AircashPayoutService : IAircashPayoutService
    {

        private readonly ISignatureService SignatureService;
        private  IHttpRequestServices HttpRequestServices;
        private readonly AircashPayoutConfiguration AircashPayoutConfiguration;
        private readonly Guid PartnerId;
        private readonly string PersonalID;

        public AircashPayoutService(
           ISignatureService signatureService,
           IOptionsMonitor<AircashPayoutConfiguration> aircashPayoutConfiguration,
           IOptionsMonitor<Configuration> configuration,
           IHttpRequestServices httpRequestServices
       )
        {
            SignatureService = signatureService;
            HttpRequestServices = httpRequestServices;
            AircashPayoutConfiguration = aircashPayoutConfiguration.CurrentValue;
            PartnerId = new Guid(configuration.CurrentValue.PartnerId);
            PersonalID = null;
        }


        public async Task<string> CheckTransactionStatus(string partnerTransactionId, string aircashTransactionId)
        {
            var sequence = $"AircashTransactionID={aircashTransactionId}&PartnerID={PartnerId}&PartnerTransactionID={partnerTransactionId}";
            sequence = $"{sequence}{AircashPayoutConfiguration.CheckTransactionStatusUrl}";

            var signature = SignatureService.GenerateAircashSignature(sequence);

            var checkTransactionStatusRequest = new
            {
                AircashTransactionID = aircashTransactionId,
                PartnerID = PartnerId,
                PartnerTransactionID = partnerTransactionId
            };

            var jsonContent = JsonConvert.SerializeObject(checkTransactionStatusRequest);
            var uri = AircashPayoutConfiguration.AircashPayoutBaseUri;

            return await HttpRequestServices.CreateAircashRequest(uri, jsonContent);
        }

        public async Task<string> CheckUser(string phoneNumber)
        {
            var sequence = $"PartnerID={PartnerId}&PersonalID=&PhoneNumber={phoneNumber}";
            
            sequence = $"{sequence}{AircashPayoutConfiguration.CheckUserUrl}";

            var signature = SignatureService.GenerateAircashSignature(sequence);

            var checkUserRequest = new CheckUserRequest { 
                PartnerID = PartnerId, 
                PersonalID = null, 
                PhoneNumber = phoneNumber,
                Signature = signature
            };

            var jsonContent= JsonConvert.SerializeObject(checkUserRequest);
            var uri = AircashPayoutConfiguration.AircashPayoutBaseUri;

            return await HttpRequestServices.CreateAircashRequest(uri, jsonContent);    
        }

        public async Task<string> CreatePayout(string partnerTransactionId, decimal amount, string phoneNumber)
        {
            var sequence = $"Amount={amount}&PartnerID={PartnerId}&PartnerTransactionID={partnerTransactionId}&PersonalID=&PhoneNumber={phoneNumber}";
            sequence = $"{sequence}{AircashPayoutConfiguration.CreatePayoutUrl}";

            var signature = SignatureService.GenerateAircashSignature(sequence);

            var createPayouRequest = new
            {
                Amount = amount,
                PartnerID = PartnerId,
                PartnerTransactionID = partnerTransactionId,
                PersonalID=PersonalID,
                PhoneNumber = phoneNumber,
                Signature = signature
            };

            var jsonContent = JsonConvert.SerializeObject(createPayouRequest);
            var uri = AircashPayoutConfiguration.AircashPayoutBaseUri;
            
            return await HttpRequestServices.CreateAircashRequest(uri, jsonContent);
            //var obj = new
            //{
            //    Vrijednost = await HttpRequestServices.CreateAircashRequest(uri, jsonContent)
            //};
        }

    }
}
