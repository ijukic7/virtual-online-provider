﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.AircashPayout
{
    public interface IAircashPayoutService
    {
        Task<string> CheckUser(string phoneNumber);
        Task<string> CreatePayout(string partnerTransactionID, decimal amount, string phoneNumber);

        Task<string>CheckTransactionStatus(string partnerTransactionId, string aircashTransactionId);
    }
}
