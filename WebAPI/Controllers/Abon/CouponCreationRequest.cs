﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers.Abon
{
    public class CouponCreationRequest
    {

        public decimal Value { get; set; }
        public string PointOfSaleId { get; set; }
        public string ISOCurrencySymbol { get; set; }

    }
}
