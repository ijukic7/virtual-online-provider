﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers.Abon
{
    public class ConfirmTransactionRequest
    {
        public string CouponCode { get; set; }
        public string ProviderTransactionId { get; set; }
        

    }
}
