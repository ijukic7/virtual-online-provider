﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers.Abon
{
    public class CouponCancellationRequest
    {
        
        public string SerialNumber { get; set; }

        public string PartnerTransactionId { get; set; }

        public string PointOfSaleId { get; set; }

        public string SingedData { get; set; }

    }
}
