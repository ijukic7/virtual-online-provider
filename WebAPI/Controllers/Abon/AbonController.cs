﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Abon;

namespace WebAPI.Controllers.Abon
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AbonController : ControllerBase
    {
        private readonly IAbonService _abonService;
        public AbonController(IAbonService abonService)
        {
            _abonService = abonService;
        }
        
        [HttpPost]
        public async Task<ActionResult> ValidateCoupon(ValidateCouponRequest request)
        {

            var response = await _abonService.ValidateCoupon(request.CouponCode);
            return Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult> ConfirmTransaction(ConfirmTransactionRequest request) 
        {
            var response = await _abonService.ConfirmTransaction(request.CouponCode,request.ProviderTransactionId);
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> CreateCoupon(CouponCreationRequest request)
        {
            var response = await _abonService.CreateCoupon(request.ISOCurrencySymbol,request.PointOfSaleId,request.Value);
            return Ok(response);
        }
        [HttpPost]
        public async Task<ActionResult> CancelCoupon(CouponCancellationRequest request)
        {
            var response = await _abonService.CancelCoupon(request.SerialNumber,request.PartnerTransactionId,request.PointOfSaleId);
            return Ok(response);
        }

    }
}