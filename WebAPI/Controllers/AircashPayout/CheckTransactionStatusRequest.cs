﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers.AircashPayment
{
    public class CheckTransactionStatusRequest
    {
        public string PartnerTransactionId { get; set; }
        public string AircashTransactionId { get; set; }
    }
}
