﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.AircashPayout;

namespace WebAPI.Controllers.AircashPayment
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AircashPayoutController : ControllerBase
    {
        private readonly IAircashPayoutService _aircashPayoutServices;

        public AircashPayoutController(IAircashPayoutService aircashPaymetServices)
        {
            _aircashPayoutServices = aircashPaymetServices;
        }


        [HttpPost]
        public async Task<ActionResult> CheckTransactionStatus(CheckTransactionStatusRequest request)
        {

            var response = await _aircashPayoutServices.CheckTransactionStatus(request.PartnerTransactionId,request.AircashTransactionId);
            return Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult> CheckUser(CheckUserRequest request)
        {

            var response = await _aircashPayoutServices.CheckUser(request.PhoneNumber);
            return Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult> CreatePayout(CreatePayoutRequest request)
        {

            var response = await _aircashPayoutServices.CreatePayout(request.PartnerTransactionId,request.Amount,request.PhoneNumber);
            return Ok(response);
        }
    }
}
