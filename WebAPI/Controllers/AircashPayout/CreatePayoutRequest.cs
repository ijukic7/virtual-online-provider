﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers.AircashPayment
{
    public class CreatePayoutRequest
    {
        public string PartnerTransactionId { get; set; }
        public decimal Amount{ get; set; }
        public string PhoneNumber { get; set; }
        public string PersonalId { get; set; }

    }
}
