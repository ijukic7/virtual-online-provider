using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Aircash.Config;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Services.Abon;
using Services.Signature;
using Services.TemplateSystem;
using Services.AircashPayout;
using Services.HttpRequest;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            
            services.AddTransient<IAbonService, AbonService>();
            services.AddTransient<ISignatureService, SignatureService>();
            services.AddTransient<ITemplateSystemService, TemplateSystemService>();
            services.AddTransient<IAircashPayoutService, AircashPayoutService>();
            services.AddTransient<IHttpRequestServices, HttpRequestServices>();

            services.Configure<AbonConfiguration>(Configuration.GetSection("AbonConfiguration"));
            services.Configure<Configuration>(Configuration.GetSection("Configuration"));
            services.Configure<AircashPayoutConfiguration>(Configuration.GetSection("AircashPayoutConfiguration"));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
