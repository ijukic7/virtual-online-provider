﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Signature
{
    public interface ISignatureService
    {
        string GenerateAbonSignature(string sequence);

        string GenerateAircashSignature(string sequence);
    }
}
