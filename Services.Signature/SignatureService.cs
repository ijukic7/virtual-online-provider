﻿using Aircash.Config;
using Microsoft.Extensions.Options;
using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;



namespace Services.Signature
{
    public class SignatureService : ISignatureService
    {
      
       private readonly Configuration Configuration;
       public SignatureService(IOptionsMonitor<Configuration> configuration)
       {
            Configuration = configuration.CurrentValue;
       }

        public string GenerateAircashSignature(string sequence)
        {
            var certificate = new X509Certificate2(Configuration.PrivateKeyPath,Configuration.PrivateKeyPass);

            string signature;
            using (RSA rsa = certificate.GetRSAPrivateKey())
            {
                var dataToSign = Encoding.UTF8.GetBytes(sequence);
                var signedData = rsa.SignData(dataToSign, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);


                signature = Convert.ToBase64String(signedData);
            }
            return signature;

        }

        public string GenerateAbonSignature(string sequence)
        {

            var certificate = new X509Certificate2(Configuration.PrivateKeyPath, Configuration.PrivateKeyPass); 
            
            var xml = "";
            using (RSA rsa = certificate.GetRSAPublicKey())
            {
                xml = rsa.ToXmlString(false);
            }

            sequence = $"{sequence}{xml}";
                        
            //STRING KOJI JA POTPISUJEM I KAO TAKAV ŠALJEM AIRCASHU
            //Ta sequenca se potpisuje s privatnim kljucem

            string signature;
            using (RSA rsa = certificate.GetRSAPrivateKey())
            {
                var dataToSign = Encoding.UTF8.GetBytes(sequence);
                var signedData = rsa.SignData(dataToSign, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);


                signature = Convert.ToBase64String(signedData);
            }
            return signature;
        }
    }
}
