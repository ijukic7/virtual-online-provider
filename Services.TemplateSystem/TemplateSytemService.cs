﻿using Stubble.Core.Builders;
using System.Threading.Tasks;

namespace Services.TemplateSystem
{
    public class TemplateSystemService : ITemplateSystemService
    {
        public async Task<string> RenderTemplateAsync(string template, object obj)
        {
            var stubble = new StubbleBuilder().Build();
            var output = await stubble.RenderAsync(template, obj);
            return output;
        }
    }
}
