﻿using System.Threading.Tasks;

namespace Services.TemplateSystem
{
    public interface ITemplateSystemService
    {
        Task<string> RenderTemplateAsync(string template, object obj);
        
    }
}
