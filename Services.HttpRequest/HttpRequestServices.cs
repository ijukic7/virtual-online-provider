﻿using Aircash.Config;
using Microsoft.Extensions.Options;
using Services.Signature;
using Services.TemplateSystem;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services.HttpRequest
{
    public class HttpRequestServices : IHttpRequestServices
    {
        public async Task<string> CreateAbonRequest(string uri, string soapEnvelopeRequest, string soapAction)
        {
            //bypass invalid SSL certificate
            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback =
                (message, certificate, chain, sslPolicyErrors) => true;

            var httpClient = new HttpClient(handler);
            string responseContent;
            using (var request = new HttpRequestMessage(HttpMethod.Post, uri))
            {
                request.Content = new StringContent(soapEnvelopeRequest, Encoding.UTF8, "text/xml");
                request.Headers.Add("SOAPAction", soapAction);
                using (var response = await httpClient.SendAsync(request))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception(response.StatusCode.ToString());
                    }

                    responseContent = await response.Content.ReadAsStringAsync();
                }
            };

            return responseContent;
        }

        public async Task<string> CreateAircashRequest(string uri, string jsonContent)
        {
            //bypass invalid SSL certificate
            var handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback =
                (message, certificate, chain, sslPolicyErrors) => true;

            var httpClient = new HttpClient(handler);
            string responseContent;
            using (var request = new HttpRequestMessage(HttpMethod.Post, uri))
            {
                request.Content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
      
                using (var response = await httpClient.SendAsync(request))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception(response.StatusCode.ToString());
                    }
                    responseContent = await response.Content.ReadAsStringAsync();
                }
            };

            return responseContent;
        }
    }
}
