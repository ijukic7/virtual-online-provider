﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.HttpRequest
{
   public interface IHttpRequestServices
    {

        Task <string> CreateAbonRequest(string uri, string soapEnvelopeRequest, string soapAction);
        Task<string> CreateAircashRequest(string uri, string jsonContent);
    }
}
