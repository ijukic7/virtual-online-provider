﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abon
{
    public interface IAbonService
    {
        Task<string> ValidateCoupon(string couponCode);
        Task<string> ConfirmTransaction(string couponCode, string providerTransactionId);
        Task<string> CreateCoupon(string ISOCurrencySymbol,string PointOfSaleId, decimal Value);
        Task<string> CancelCoupon(string SerialNumber, string PartnerTransactionId, string PointOfSaleId);
    }
}
