﻿using Aircash.Config;
using Microsoft.Extensions.Options;
using Services.Signature;
using Services.TemplateSystem;
using Services.HttpRequest;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abon
{
    public class AbonService : IAbonService
    {
        private readonly ISignatureService SignatureService;
        private readonly ITemplateSystemService TemplateSystemService;
        private readonly IHttpRequestServices HttpRequestServices;
        private readonly AbonConfiguration AbonConfiguration;
        private readonly Guid PartnerId;
        

        public AbonService(
            ISignatureService signatureService, 
            ITemplateSystemService templateSystemService, 
            IHttpRequestServices httpRequestServices,
            IOptionsMonitor<AbonConfiguration> abonConfiguration,
            IOptionsMonitor<Configuration>configuration
        )
        {
            SignatureService = signatureService;
            TemplateSystemService = templateSystemService;
            HttpRequestServices = httpRequestServices;
            AbonConfiguration = abonConfiguration.CurrentValue;
            PartnerId = new Guid(configuration.CurrentValue.PartnerId);
        }
        

        public async Task<string> ValidateCoupon(string couponCode)
        {
            var sequence = $"{couponCode.ToUpper()}{PartnerId.ToString().ToUpper()}";
            var signature = SignatureService.GenerateAbonSignature(sequence);

            var template = AbonConfiguration.ValidateCouponSoapEnvelope;
            var obj = new { 
                CouponCode = couponCode.ToUpper(),
                ProviderId = PartnerId,
                Signature = signature
            };
            var soapEnvelope = await TemplateSystemService.RenderTemplateAsync(template, obj);
            var uri = AbonConfiguration.AbonSoapUri;
            var soapAction = AbonConfiguration.ValidateCouponSoapAction;

            return await HttpRequestServices.CreateAbonRequest(uri, soapEnvelope, soapAction);
        }

        public async Task<string> ConfirmTransaction(string couponCode,string providerTransactionId) 
        {
            var sequence = $"{couponCode.ToString().ToUpper()}{PartnerId.ToString().ToUpper()}{providerTransactionId.ToUpper()}";
            var signature = SignatureService.GenerateAbonSignature(sequence);

            var template = AbonConfiguration.ConfirmTransactionSoapEnvelope;
            var obj = new
            {
                CouponCode = couponCode,
                ProviderId = PartnerId,
                ProviderTransactionId=providerTransactionId,
                Signature = signature
            };
            var soapEnvelope = await TemplateSystemService.RenderTemplateAsync(template, obj);
            var uri = AbonConfiguration.AbonSoapUri;
            var soapAction = AbonConfiguration.ValidateCouponSoapAction;

            return await HttpRequestServices.CreateAbonRequest(uri, soapEnvelope, soapAction);
        }



        public async Task<string> CreateCoupon(string iSOCurrencySymbol, string pointOfSaleId, decimal value)
        {
            var partnerTransactionId = Guid.NewGuid();
            var sequence = $"{PartnerId.ToString().ToUpper()}{value.ToString().ToUpper()}{pointOfSaleId.ToUpper()}{iSOCurrencySymbol.ToUpper()}{partnerTransactionId.ToString().ToUpper()}";
            var signature = SignatureService.GenerateAbonSignature(sequence);

            var template = AbonConfiguration.CreateCouponSoapEnvelope;
            var obj = new
            {
                ISOCurrencySymbol=iSOCurrencySymbol,
                PartnerId = PartnerId,
                PartnerTransactionId=partnerTransactionId,
                PointOfSaleId=pointOfSaleId,
                Signature = signature,
                Value=value
            };

            var soapEnvelope = await TemplateSystemService.RenderTemplateAsync(template, obj);
            var uri = AbonConfiguration.AbonSoapUriSalePartner;
            var soapAction = AbonConfiguration.CreateCouponSoapAction;

            return await HttpRequestServices.CreateAbonRequest(uri,soapEnvelope,soapAction); 
        }

        public async Task<string> CancelCoupon(string serialNumber, string partnerTransactionId, string pointOfSaleId)
        {
            
            var sequence = $"{PartnerId.ToString().ToUpper()}{serialNumber.ToUpper()}{pointOfSaleId.ToUpper()}{partnerTransactionId.ToUpper()}";
            var signature = SignatureService.GenerateAbonSignature(sequence);

            var template = AbonConfiguration.CancelCouponSoapEnvelope;
            var obj = new
            {
                ProviderId = PartnerId,
                PartnerTransactionId= partnerTransactionId,
                PointOfSaleId = pointOfSaleId,
                SerialNumber = serialNumber,
                Signature = signature
            };
            var soapEnvelope = await TemplateSystemService.RenderTemplateAsync(template, obj);
            var uri = AbonConfiguration.AbonSoapUri;
            var soapAction = AbonConfiguration.CancelCouponSoapAction;

            return await HttpRequestServices.CreateAbonRequest(uri, soapEnvelope, soapAction);
        }
    }
}
